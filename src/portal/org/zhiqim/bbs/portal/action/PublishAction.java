/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。

 * 
 * 指定登记&发行网站： https://www.zhiqim.com/ 欢迎加盟知启蒙，[编程有你，知启蒙一路随行]。
 *
 * 本文采用《知启蒙登记发行许可证》，除非符合许可证，否则不可使用该文件！
 * 1、您可以免费使用、修改、合并、出版发行和分发，再授权软件、软件副本及衍生软件；
 * 2、您用于商业用途时，必须在原作者指定的登记网站，按原作者要求进行登记；
 * 3、您在使用、修改、合并、出版发行和分发时，必须包含版权声明、许可声明，及保留原作者的著作权、商标和专利等知识产权；
 * 4、您在互联网、移动互联网等大众网络下发行和分发再授权软件、软件副本及衍生软件时，必须在原作者指定的发行网站进行发行和分发；
 * 5、您可以在以下链接获取一个完整的许可证副本。
 * 
 * 许可证链接：http://zhiqim.org/licenses/zhiqim_register_publish_license.htm
 *
 * 除非法律需要或书面同意，软件由原始码方式提供，无任何明示或暗示的保证和条件。详见完整许可证的权限和限制。
 */
package org.zhiqim.bbs.portal.action;

import java.util.List;

import org.zhiqim.account.ZacSessionUser;
import org.zhiqim.bbs.dbo.BbsBoard;
import org.zhiqim.bbs.dbo.BbsThread;
import org.zhiqim.bbs.dbo.BbsTopic;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.extend.GetPostAction;
import org.zhiqim.httpd.util.Sessions;
import org.zhiqim.httpd.validate.ones.IsLen;
import org.zhiqim.httpd.validate.ones.IsNotEmpty;
import org.zhiqim.httpd.validate.ones.IsNumericLen;
import org.zhiqim.kernel.util.DateTimes;
import org.zhiqim.kernel.util.Ids;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;

public class PublishAction extends GetPostAction
{
    @Override
    protected void validate(HttpRequest request)
    {// 发帖页面和处理验证
        request.addValidate(new IsNotEmpty("boardId", "版块编号不能为空"));
        request.addValidate(new IsLen("topicTitle", "主题不能为空且不超过100汉字", 1, 100));
        request.addValidate(new IsLen("threadContent", "帖子内容不能为空且不超过4000汉字", 1, 4000));
        request.addValidate(new IsNumericLen("verificationCode", "验证码必须是4位数字", 4, 4));
    }

    @Override
    protected void doGet(HttpRequest request) throws Exception
    {// 发帖页面
        List<BbsBoard> boardList = ORM.get(ZTable.class).list(BbsBoard.class);
        request.setAttribute("boardList", boardList);
    }

    @Override
    protected void doPost(HttpRequest request) throws Exception
    {// 发帖处理
        String verificationCode = request.getParameter("verificationCode");
        String vcode = Sessions.getSessionVerificationCode(request);
        if (!verificationCode.equals(vcode))
        {
            request.returnHistory("验证码不正确或已失效，请新输入！");
            return;
        }

        long boardId = request.getParameterLong("boardId");
        long userId = request.getSessionUser(ZacSessionUser.class).getUserId();
        String topicTitle = request.getParameter("topicTitle");
        String threadContent = request.getParameterNoFilter("threadContent");
        long topicId = Ids.longId();

        // 插入到BBS_TOPIC数据库
        BbsTopic bbsTopic = new BbsTopic();
        bbsTopic.setBoardId(boardId);
        bbsTopic.setTopicId(topicId);
        bbsTopic.setTopicTop(false);
        bbsTopic.setTopicCreated(DateTimes.getDateTimeString());
        bbsTopic.setTopicTitle(topicTitle);
        bbsTopic.setTopicUserId(userId);
        bbsTopic.setTopicReplyNum(0);
        bbsTopic.setTopicReadNum(0);
        bbsTopic.setTopicLastReplyTime(DateTimes.getDateTimeString());
        bbsTopic.setTopicLastReplyAuthor(userId);

        ORM.get(ZTable.class).insert(bbsTopic);

        // 插入到BBS_THREAD数据库
        BbsThread bbsThread = new BbsThread();
        bbsThread.setBoardId(boardId);
        bbsThread.setTopicId(topicId);
        bbsThread.setThreadNum(0);
        bbsThread.setThreadCreated(DateTimes.getDateTimeString());
        bbsThread.setThreadUserId(userId);
        bbsThread.setThreadContent(threadContent);

        ORM.get(ZTable.class).insert(bbsThread);

        request.setRedirect("/bbs/topic.htm?boardId=" + boardId);
    }
}
