/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。

 * 
 * 指定登记&发行网站： https://www.zhiqim.com/ 欢迎加盟知启蒙，[编程有你，知启蒙一路随行]。
 *
 * 本文采用《知启蒙登记发行许可证》，除非符合许可证，否则不可使用该文件！
 * 1、您可以免费使用、修改、合并、出版发行和分发，再授权软件、软件副本及衍生软件；
 * 2、您用于商业用途时，必须在原作者指定的登记网站，按原作者要求进行登记；
 * 3、您在使用、修改、合并、出版发行和分发时，必须包含版权声明、许可声明，及保留原作者的著作权、商标和专利等知识产权；
 * 4、您在互联网、移动互联网等大众网络下发行和分发再授权软件、软件副本及衍生软件时，必须在原作者指定的发行网站进行发行和分发；
 * 5、您可以在以下链接获取一个完整的许可证副本。
 * 
 * 许可证链接：http://zhiqim.org/licenses/zhiqim_register_publish_license.htm
 *
 * 除非法律需要或书面同意，软件由原始码方式提供，无任何明示或暗示的保证和条件。详见完整许可证的权限和限制。
 */
package org.zhiqim.bbs.portal.action;

import org.zhiqim.account.ZacSessionUser;
import org.zhiqim.bbs.dbo.BbsThread;
import org.zhiqim.bbs.dbo.BbsTopic;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.util.Sessions;
import org.zhiqim.httpd.validate.ones.IsIntegerValue;
import org.zhiqim.httpd.validate.ones.IsLen;
import org.zhiqim.httpd.validate.ones.IsNotEmpty;
import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.util.DateTimes;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;
import org.zhiqim.orm.dbo.Selector;
import org.zhiqim.orm.dbo.Updater;

@AnAlias("BBSModel")
public class BBSModel
{
    /** 主题置顶 */
    public static void doTopicTop(HttpRequest request) throws Exception
    {
        //检查参数
        request.addValidate(new IsNotEmpty("topicId", "请选择一个主题"));
        if (!request.chkValidate())
        {
            request.setResponseError(request.getAlertMsg());
            return;
        }
        
        long topicId = request.getParameterLong("topicId");

        //更新BBS_TOPIC表
        Updater updater = new Updater();
        updater.addMust("topicId", topicId);
        updater.addField("topicTop", true);
        ORM.get(ZTable.class).update(BbsTopic.class, updater);
    }

    /** 主题置顶取消 */
    public static void doTopicTopCancel(HttpRequest request) throws Exception
    {
        //检查参数
        request.addValidate(new IsNotEmpty("topicId", "请选择一个主题"));
        if (!request.chkValidate())
        {
            request.setResponseError(request.getAlertMsg());
            return;
        }
        
        long topicId = request.getParameterLong("topicId");

        //更新BBS_TOPIC表
        Updater updater = new Updater();
        updater.addMust("topicId", topicId);
        updater.addField("topicTop", false);
        ORM.get(ZTable.class).update(BbsTopic.class, updater);
    }

    /** 删除主题 */
    public static void doDelTopic(HttpRequest request) throws Exception
    {
        //检查参数
        request.addValidate(new IsNotEmpty("topicId", "请选择一个主题"));
        if (!request.chkValidate())
        {
            request.setResponseError(request.getAlertMsg());
            return;
        }
        
        Long topicId = request.getParameterLong("topicId");
        
        ORM.get(ZTable.class).delete(BbsTopic.class, topicId);
        ORM.get(ZTable.class).delete(BbsThread.class, new Selector("topicId", topicId));
    }

    /** 删除楼层 */
    public static void doDelReply(HttpRequest request) throws Exception
    {
        //检查参数
        request.addValidate(new IsNotEmpty("topicId", "请选择一个主题"));
        request.addValidate(new IsNotEmpty("threadNum", "请选择一个楼层"));
        if (!request.chkValidate())
        {
            request.setResponseError(request.getAlertMsg());
            return;
        }
        
        long topicId = request.getParameterLong("topicId");
        int threadNum = request.getParameterInt("threadNum");
        
        Selector selector = new Selector();
        selector.addMust("topicId", topicId);
        selector.addMust("threadNum", threadNum);
        ORM.get(ZTable.class).delete(BbsThread.class, selector);
    }
    
    /**回复帖子*/
    public void doReply(HttpRequest request)throws Exception
    {
        ZacSessionUser sessionUser = (ZacSessionUser) request.getSessionUser();
        if(sessionUser == null)
        {
            request.setResponseError("请先登录，再回复");
            return;
        }
        
        request.addValidate(new IsNotEmpty("topicId", "回复的帖子不存在，请刷新页面"));
        request.addValidate(new IsLen("threadContent", "请输入至少5个字符的回复内容", 5, 2000));
        request.addValidate(new IsIntegerValue("verificationCode", "请输入正确的验证码", 1000, 9999));
        if (!request.chkValidate())
        {
            request.setResponseError(request.getAlertMsg());
            return;
        }
        
        long userId = sessionUser.getUserId();
        
        String verificationCode = request.getParameter("verificationCode");
        String threadContent = request.getParameterNoFilter("threadContent");
        long topicId = request.getParameterLong("topicId");
        
        String vcode = Sessions.getSessionVerificationCode(request);
        if (!verificationCode.equals(vcode))
        {
            request.setResponseError("验证码不正确或已失效，请新输入！");
            return;
        }
        
        String curTime = DateTimes.getDateTimeString();
        BbsTopic bbsTopic = ORM.get(ZTable.class).item(BbsTopic.class, topicId);
        int topicReplyNum = bbsTopic.getTopicReplyNum();
        
        BbsThread bbsThread = new BbsThread();
        bbsThread.setBoardId(bbsTopic.getBoardId());
        bbsThread.setTopicId(topicId);
        bbsThread.setThreadNum(topicReplyNum + 1);
        bbsThread.setThreadCreated(curTime);
        bbsThread.setThreadUserId(userId);
        bbsThread.setThreadContent(threadContent);

        Updater updater = new Updater();
        updater.addMust("topicId", topicId);
        updater.addField("topicReplyNum", topicReplyNum + 1);
        updater.addField("topicLastReplyTime", curTime);
        updater.addField("topicLastReplyAuthor", userId);
        
        ORM.get(ZTable.class).insert(bbsThread);
        ORM.get(ZTable.class).update(BbsTopic.class, updater);
      
    }
}
