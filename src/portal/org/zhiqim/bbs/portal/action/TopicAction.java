/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。

 * 
 * 指定登记&发行网站： https://www.zhiqim.com/ 欢迎加盟知启蒙，[编程有你，知启蒙一路随行]。
 *
 * 本文采用《知启蒙登记发行许可证》，除非符合许可证，否则不可使用该文件！
 * 1、您可以免费使用、修改、合并、出版发行和分发，再授权软件、软件副本及衍生软件；
 * 2、您用于商业用途时，必须在原作者指定的登记网站，按原作者要求进行登记；
 * 3、您在使用、修改、合并、出版发行和分发时，必须包含版权声明、许可声明，及保留原作者的著作权、商标和专利等知识产权；
 * 4、您在互联网、移动互联网等大众网络下发行和分发再授权软件、软件副本及衍生软件时，必须在原作者指定的发行网站进行发行和分发；
 * 5、您可以在以下链接获取一个完整的许可证副本。
 * 
 * 许可证链接：http://zhiqim.org/licenses/zhiqim_register_publish_license.htm
 *
 * 除非法律需要或书面同意，软件由原始码方式提供，无任何明示或暗示的保证和条件。详见完整许可证的权限和限制。
 */
package org.zhiqim.bbs.portal.action;

import java.util.ArrayList;
import java.util.List;

import org.zhiqim.bbs.dbo.BbsBoard;
import org.zhiqim.bbs.ex.BbsTopicEx;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.core.Action;
import org.zhiqim.kernel.paging.PageBuilder;
import org.zhiqim.kernel.paging.PageResult;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;
import org.zhiqim.orm.ZView;
import org.zhiqim.orm.dbo.Selector;

/**
 * 主题列表
 * @author zhouwenbin
 *
 */
public class TopicAction implements Action
{
    @Override
    public void execute(HttpRequest request) throws Exception
    {
        int page = request.getParameterInt("page", 1);
        int pageSize = request.getContextAttributeInt("fmr_page_size", 10);
        long boardId = request.getParameterLong("boardId");

        BbsBoard bbsBoard = ORM.get(ZTable.class).item(BbsBoard.class, boardId);
        request.setAttribute("bbsBoard", bbsBoard);
        
        //置顶列表
        Selector selector1 = new Selector();
        selector1.addMust("boardId", boardId);
        selector1.addMust("topicTop", true);
        selector1.addOrderbyDesc("topicCreated");
        List<BbsTopicEx> bbsTopic1 = ORM.get(ZView.class).list(BbsTopicEx.class, selector1);

        Selector selector2 = new Selector();
        selector2.addMust("boardId", boardId);
        selector2.addMust("topicTop", false); 
        selector2.addOrderbyDesc("topicCreated");
        List<BbsTopicEx> bbsTopic2 = ORM.get(ZView.class).list(BbsTopicEx.class, selector2);

        List<BbsTopicEx> tempList = new ArrayList<>();
        tempList.addAll(bbsTopic1);
        tempList.addAll(bbsTopic2);
        
        PageResult<BbsTopicEx> result = PageBuilder.pageResult(page, pageSize, tempList);
        result.addConditionMap(request.getParameterMap());
        request.setAttribute("result", result);
    }
}
