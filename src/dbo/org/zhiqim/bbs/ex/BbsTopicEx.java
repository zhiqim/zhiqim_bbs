/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。
 * 
 * 指定登记&发行网站： https://www.zhiqim.com/ 欢迎加盟知启蒙，[编程有你，知启蒙一路随行]。
 *
 * 本文采用《知启蒙登记发行许可证》，除非符合许可证，否则不可使用该文件！
 * 1、您可以免费使用、修改、合并、出版发行和分发，再授权软件、软件副本及衍生软件；
 * 2、您用于商业用途时，必须在原作者指定的登记网站，按原作者要求进行登记；
 * 3、您在使用、修改、合并、出版发行和分发时，必须包含版权声明、许可声明，及保留原作者的著作权、商标和专利等知识产权；
 * 4、您在互联网、移动互联网等大众网络下发行和分发再授权软件、软件副本及衍生软件时，必须在原作者指定的发行网站进行发行和分发；
 * 5、您可以在以下链接获取一个完整的许可证副本。
 * 
 * 许可证链接：http://zhiqim.org/licenses/zhiqim_register_publish_license.htm
 *
 * 除非法律需要或书面同意，软件由原始码方式提供，无任何明示或暗示的保证和条件。详见完整许可证的权限和限制。
 */
package org.zhiqim.bbs.ex;

import org.zhiqim.bbs.dbo.BbsTopic;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 主题扩展表 对应视图《BBS_TOPIC_EX》
 */
@AnAlias("BbsTopicEx")
@AnNew
@AnView("BBS_TOPIC,USER_INFO a,USER_INFO b,BBS_BOARD")
@AnViewJoin({@AnViewJoinValue(type="EQUAL", lTable="BBS_TOPIC", lColumn="BOARD_ID", rTable="BBS_BOARD", rColumn="BOARD_ID"),
             @AnViewJoinValue(type="EQUAL", lTable="BBS_TOPIC", lColumn="TOPIC_USER_ID", rTable="a", rColumn="USER_ID"),
             @AnViewJoinValue(type="LEFT", lTable="BBS_TOPIC", lColumn="TOPIC_LAST_REPLY_AUTHOR", rTable="b", rColumn="USER_ID")})
public class BbsTopicEx extends BbsTopic
{
    private static final long serialVersionUID = 1L;

    @AnViewField(table="a", column="USER_NICK")    private String userNickA;    //1.
    @AnViewField(table="a", column="USER_AVATAR_50")    private String userAvatar50A;    //2.
    @AnViewField(table="b", column="USER_NICK")    private String userNickB;    //3.
    @AnViewField(table="b", column="USER_AVATAR_50")    private String userAvatar50B;    //4.
    @AnViewField(table="BBS_BOARD", column="BOARD_MANAGER")    private long boardManager;    //5.板块管理员
    @AnViewField(table="BBS_BOARD", column="BOARD_NAME")    private String boardName;    //6.板块名称

    public String toString()
    {
        return Jsons.toString(this);
    }

    public String getUserNickA()
    {
        return userNickA;
    }

    public void setUserNickA(String userNickA)
    {
        this.userNickA = userNickA;
    }

    public String getUserAvatar50A()
    {
        return userAvatar50A;
    }

    public void setUserAvatar50A(String userAvatar50A)
    {
        this.userAvatar50A = userAvatar50A;
    }

    public String getUserNickB()
    {
        return userNickB;
    }

    public void setUserNickB(String userNickB)
    {
        this.userNickB = userNickB;
    }

    public String getUserAvatar50B()
    {
        return userAvatar50B;
    }

    public void setUserAvatar50B(String userAvatar50B)
    {
        this.userAvatar50B = userAvatar50B;
    }

    public long getBoardManager()
    {
        return boardManager;
    }

    public void setBoardManager(long boardManager)
    {
        this.boardManager = boardManager;
    }

    public String getBoardName()
    {
        return boardName;
    }

    public void setBoardName(String boardName)
    {
        this.boardName = boardName;
    }

}
