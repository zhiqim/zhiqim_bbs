/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。
 * 
 * 指定登记&发行网站： https://www.zhiqim.com/ 欢迎加盟知启蒙，[编程有你，知启蒙一路随行]。
 *
 * 本文采用《知启蒙登记发行许可证》，除非符合许可证，否则不可使用该文件！
 * 1、您可以免费使用、修改、合并、出版发行和分发，再授权软件、软件副本及衍生软件；
 * 2、您用于商业用途时，必须在原作者指定的登记网站，按原作者要求进行登记；
 * 3、您在使用、修改、合并、出版发行和分发时，必须包含版权声明、许可声明，及保留原作者的著作权、商标和专利等知识产权；
 * 4、您在互联网、移动互联网等大众网络下发行和分发再授权软件、软件副本及衍生软件时，必须在原作者指定的发行网站进行发行和分发；
 * 5、您可以在以下链接获取一个完整的许可证副本。
 * 
 * 许可证链接：http://zhiqim.org/licenses/zhiqim_register_publish_license.htm
 *
 * 除非法律需要或书面同意，软件由原始码方式提供，无任何明示或暗示的保证和条件。详见完整许可证的权限和限制。
 */
package org.zhiqim.bbs.ex;

import org.zhiqim.bbs.dbo.BbsBoard;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 板块扩展表 对应视图《BBS_BOARD_EX》
 */
@AnAlias("BbsBoardEx")
@AnNew
@AnView("BBS_BOARD,USER_INFO")
@AnViewJoin({@AnViewJoinValue(type="EQUAL", lTable="BBS_BOARD", lColumn="BOARD_MANAGER", rTable="USER_INFO", rColumn="USER_ID")})
public class BbsBoardEx extends BbsBoard
{
    private static final long serialVersionUID = 1L;

    @AnViewField(table="USER_INFO", column="USER_NICK")    private String userNick;    //1.用户昵称，默认等于用户编号，系统内唯一
    @AnViewField(table="USER_INFO", column="USER_MOBILE")    private String userMobile;    //2.用户手机，一个手机最多绑定5个用户
    @AnViewField(table="USER_INFO", column="USER_EMAIL")    private String userEmail;    //3.用户邮箱，一个邮箱最多绑定5个账号
    @AnViewField(table="USER_INFO", column="USER_AVATAR_50")    private String userAvatar50;    //4.用户头像
    @AnViewField(table="USER_INFO", column="USER_AVATAR_100")    private String userAvatar100;    //5.用户头像
    @AnViewField(table="USER_INFO", column="USER_AVATAR_150")    private String userAvatar150;    //6.用户头像

    public String toString()
    {
        return Jsons.toString(this);
    }

    public String getUserNick()
    {
        return userNick;
    }

    public void setUserNick(String userNick)
    {
        this.userNick = userNick;
    }

    public String getUserMobile()
    {
        return userMobile;
    }

    public void setUserMobile(String userMobile)
    {
        this.userMobile = userMobile;
    }

    public String getUserEmail()
    {
        return userEmail;
    }

    public void setUserEmail(String userEmail)
    {
        this.userEmail = userEmail;
    }

    public String getUserAvatar50()
    {
        return userAvatar50;
    }

    public void setUserAvatar50(String userAvatar50)
    {
        this.userAvatar50 = userAvatar50;
    }

    public String getUserAvatar100()
    {
        return userAvatar100;
    }

    public void setUserAvatar100(String userAvatar100)
    {
        this.userAvatar100 = userAvatar100;
    }

    public String getUserAvatar150()
    {
        return userAvatar150;
    }

    public void setUserAvatar150(String userAvatar150)
    {
        this.userAvatar150 = userAvatar150;
    }

}
