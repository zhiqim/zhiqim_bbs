/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。
 * 
 * 指定登记&发行网站： https://www.zhiqim.com/ 欢迎加盟知启蒙，[编程有你，知启蒙一路随行]。
 *
 * 本文采用《知启蒙登记发行许可证》，除非符合许可证，否则不可使用该文件！
 * 1、您可以免费使用、修改、合并、出版发行和分发，再授权软件、软件副本及衍生软件；
 * 2、您用于商业用途时，必须在原作者指定的登记网站，按原作者要求进行登记；
 * 3、您在使用、修改、合并、出版发行和分发时，必须包含版权声明、许可声明，及保留原作者的著作权、商标和专利等知识产权；
 * 4、您在互联网、移动互联网等大众网络下发行和分发再授权软件、软件副本及衍生软件时，必须在原作者指定的发行网站进行发行和分发；
 * 5、您可以在以下链接获取一个完整的许可证副本。
 * 
 * 许可证链接：http://zhiqim.org/licenses/zhiqim_register_publish_license.htm
 *
 * 除非法律需要或书面同意，软件由原始码方式提供，无任何明示或暗示的保证和条件。详见完整许可证的权限和限制。
 */
package org.zhiqim.bbs.dbo;

import java.io.Serializable;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 论坛帖子表 对应表《BBS_THREAD》
 */
@AnAlias("BbsThread")
@AnNew
@AnTable(table="BBS_THREAD", key="TOPIC_ID,THREAD_NUM", type="InnoDB")
public class BbsThread implements Serializable
{
    private static final long serialVersionUID = 1L;

    @AnTableField(column="BOARD_ID", type="long", notNull=true)    private long boardId;    //1.版块编号
    @AnTableField(column="TOPIC_ID", type="long", notNull=true)    private long topicId;    //2.主题编号
    @AnTableField(column="THREAD_USER_ID", type="long", notNull=true)    private long threadUserId;    //3.楼层作者
    @AnTableField(column="THREAD_NUM", type="int", notNull=true)    private int threadNum;    //4.帖子楼数
    @AnTableField(column="THREAD_CREATED", type="string,19,char", notNull=true)    private String threadCreated;    //5.帖子发表时间
    @AnTableField(column="THREAD_CONTENT", type="string,4000", notNull=true)    private String threadContent;    //6.帖子内容

    public String toString()
    {
        return Jsons.toString(this);
    }

    public long getBoardId()
    {
        return boardId;
    }

    public void setBoardId(long boardId)
    {
        this.boardId = boardId;
    }

    public long getTopicId()
    {
        return topicId;
    }

    public void setTopicId(long topicId)
    {
        this.topicId = topicId;
    }

    public long getThreadUserId()
    {
        return threadUserId;
    }

    public void setThreadUserId(long threadUserId)
    {
        this.threadUserId = threadUserId;
    }

    public int getThreadNum()
    {
        return threadNum;
    }

    public void setThreadNum(int threadNum)
    {
        this.threadNum = threadNum;
    }

    public String getThreadCreated()
    {
        return threadCreated;
    }

    public void setThreadCreated(String threadCreated)
    {
        this.threadCreated = threadCreated;
    }

    public String getThreadContent()
    {
        return threadContent;
    }

    public void setThreadContent(String threadContent)
    {
        this.threadContent = threadContent;
    }

}
