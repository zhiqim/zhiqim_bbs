/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。
 * 
 * 指定登记&发行网站： https://www.zhiqim.com/ 欢迎加盟知启蒙，[编程有你，知启蒙一路随行]。
 *
 * 本文采用《知启蒙登记发行许可证》，除非符合许可证，否则不可使用该文件！
 * 1、您可以免费使用、修改、合并、出版发行和分发，再授权软件、软件副本及衍生软件；
 * 2、您用于商业用途时，必须在原作者指定的登记网站，按原作者要求进行登记；
 * 3、您在使用、修改、合并、出版发行和分发时，必须包含版权声明、许可声明，及保留原作者的著作权、商标和专利等知识产权；
 * 4、您在互联网、移动互联网等大众网络下发行和分发再授权软件、软件副本及衍生软件时，必须在原作者指定的发行网站进行发行和分发；
 * 5、您可以在以下链接获取一个完整的许可证副本。
 * 
 * 许可证链接：http://zhiqim.org/licenses/zhiqim_register_publish_license.htm
 *
 * 除非法律需要或书面同意，软件由原始码方式提供，无任何明示或暗示的保证和条件。详见完整许可证的权限和限制。
 */
package org.zhiqim.bbs.dbo;

import java.io.Serializable;

import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.json.Jsons;
import org.zhiqim.orm.annotation.*;

/**
 * 论坛主题表 对应表《BBS_TOPIC》
 */
@AnAlias("BbsTopic")
@AnNew
@AnTable(table="BBS_TOPIC", key="TOPIC_ID", type="InnoDB")
public class BbsTopic implements Serializable
{
    private static final long serialVersionUID = 1L;

    @AnTableField(column="BOARD_ID", type="long", notNull=true)    private long boardId;    //1.版块编号
    @AnTableField(column="TOPIC_ID", type="long", notNull=true)    private long topicId;    //2.主题编号
    @AnTableField(column="TOPIC_USER_ID", type="long", notNull=true)    private long topicUserId;    //3.主题作者编号
    @AnTableField(column="TOPIC_TOP", type="boolean", notNull=true)    private boolean topicTop;    //4.主题置顶
    @AnTableField(column="TOPIC_TITLE", type="string,100", notNull=true)    private String topicTitle;    //5.主题标题
    @AnTableField(column="TOPIC_REPLY_NUM", type="int", notNull=true)    private int topicReplyNum;    //6.主题回复数
    @AnTableField(column="TOPIC_READ_NUM", type="int", notNull=true)    private int topicReadNum;    //7.主题阅读数
    @AnTableField(column="TOPIC_CREATED", type="string,19,char", notNull=true)    private String topicCreated;    //8.主题发表时间
    @AnTableField(column="TOPIC_LAST_REPLY_TIME", type="string,19,char", notNull=true)    private String topicLastReplyTime;    //9.主题最后回复时间
    @AnTableField(column="TOPIC_LAST_REPLY_AUTHOR", type="long", notNull=true)    private long topicLastReplyAuthor;    //10.主题最后回复作者

    public String toString()
    {
        return Jsons.toString(this);
    }

    public long getBoardId()
    {
        return boardId;
    }

    public void setBoardId(long boardId)
    {
        this.boardId = boardId;
    }

    public long getTopicId()
    {
        return topicId;
    }

    public void setTopicId(long topicId)
    {
        this.topicId = topicId;
    }

    public long getTopicUserId()
    {
        return topicUserId;
    }

    public void setTopicUserId(long topicUserId)
    {
        this.topicUserId = topicUserId;
    }

    public boolean isTopicTop()
    {
        return topicTop;
    }

    public void setTopicTop(boolean topicTop)
    {
        this.topicTop = topicTop;
    }

    public String getTopicTitle()
    {
        return topicTitle;
    }

    public void setTopicTitle(String topicTitle)
    {
        this.topicTitle = topicTitle;
    }

    public int getTopicReplyNum()
    {
        return topicReplyNum;
    }

    public void setTopicReplyNum(int topicReplyNum)
    {
        this.topicReplyNum = topicReplyNum;
    }

    public int getTopicReadNum()
    {
        return topicReadNum;
    }

    public void setTopicReadNum(int topicReadNum)
    {
        this.topicReadNum = topicReadNum;
    }

    public String getTopicCreated()
    {
        return topicCreated;
    }

    public void setTopicCreated(String topicCreated)
    {
        this.topicCreated = topicCreated;
    }

    public String getTopicLastReplyTime()
    {
        return topicLastReplyTime;
    }

    public void setTopicLastReplyTime(String topicLastReplyTime)
    {
        this.topicLastReplyTime = topicLastReplyTime;
    }

    public long getTopicLastReplyAuthor()
    {
        return topicLastReplyAuthor;
    }

    public void setTopicLastReplyAuthor(long topicLastReplyAuthor)
    {
        this.topicLastReplyAuthor = topicLastReplyAuthor;
    }

}
