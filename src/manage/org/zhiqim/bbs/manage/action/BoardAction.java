/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。

 * 
 * 指定登记&发行网站： https://www.zhiqim.com/ 欢迎加盟知启蒙，[编程有你，知启蒙一路随行]。
 *
 * 本文采用《知启蒙登记发行许可证》，除非符合许可证，否则不可使用该文件！
 * 1、您可以免费使用、修改、合并、出版发行和分发，再授权软件、软件副本及衍生软件；
 * 2、您用于商业用途时，必须在原作者指定的登记网站，按原作者要求进行登记；
 * 3、您在使用、修改、合并、出版发行和分发时，必须包含版权声明、许可声明，及保留原作者的著作权、商标和专利等知识产权；
 * 4、您在互联网、移动互联网等大众网络下发行和分发再授权软件、软件副本及衍生软件时，必须在原作者指定的发行网站进行发行和分发；
 * 5、您可以在以下链接获取一个完整的许可证副本。
 * 
 * 许可证链接：http://zhiqim.org/licenses/zhiqim_register_publish_license.htm
 *
 * 除非法律需要或书面同意，软件由原始码方式提供，无任何明示或暗示的保证和条件。详见完整许可证的权限和限制。
 */
package org.zhiqim.bbs.manage.action;

import org.zhiqim.account.dbo.user.UserInfo;
import org.zhiqim.bbs.dbo.BbsBoard;
import org.zhiqim.bbs.ex.BbsBoardEx;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.extend.StdSwitchAction;
import org.zhiqim.httpd.validate.ones.IsLen;
import org.zhiqim.httpd.validate.ones.IsNotEmpty;
import org.zhiqim.kernel.paging.PageResult;
import org.zhiqim.kernel.util.Ids;
import org.zhiqim.kernel.util.Validates;
import org.zhiqim.orm.ORM;
import org.zhiqim.orm.ZTable;
import org.zhiqim.orm.ZView;
import org.zhiqim.orm.dbo.Selector;
import org.zhiqim.orm.dbo.Updater;

public class BoardAction extends StdSwitchAction
{
    @Override
    protected void validateId(HttpRequest request)
    {
        request.addValidate(new IsNotEmpty("boardId", "请选择一个选项"));
    }

    @Override
    protected void validateForm(HttpRequest request)
    {
        request.addValidate(new IsLen("boardName", "板块名称不能为空且不能超过32个字符", 1, 32));
        request.addValidate(new IsNotEmpty("boardManager", "请填写正确的注册邮箱或手机号"));
    }

    protected void item(HttpRequest request) throws Exception
    {
        long boardId = request.getParameterLong("boardId");

        BbsBoard board = ORM.get(ZTable.class).item(BbsBoard.class, boardId);
        if (board == null)
        {
            request.returnHistory("该板块不存在，请重新选择");
            return;
        }
        request.setAttribute("board", board);
    }

    @Override
    protected void list(HttpRequest request) throws Exception
    {
        int page = request.getParameterInt("page", 1);
        int pageSize = request.getContextAttributeInt("fmr_page_size", 10);
        
        PageResult<BbsBoardEx> pageResult = ORM.get(ZView.class).page(BbsBoardEx.class, page, pageSize);
        pageResult.addConditionMap(request.getParameterMap());
        request.setAttribute("pageResult", pageResult);
    }

    @Override
    protected void add(HttpRequest request) throws Exception
    {
    }

    @Override
    protected void modify(HttpRequest request) throws Exception
    {
        long boardId = request.getParameterLong("boardId");
        Selector selecter = new Selector();
        selecter.addMust("boardId", boardId);
        BbsBoardEx board = ORM.get(ZView.class).item(BbsBoardEx.class, selecter);
        if (board == null)
        {
            request.returnHistory("该板块不存在，请重新选择");
            return;
        }
        request.setAttribute("board", board);
    }

    @Override
    protected void insert(HttpRequest request) throws Exception

    {
        String boardName = request.getParameter("boardName");
        String userName = request.getParameter("boardManager");
        String boardDesc = request.getParameter("boardDesc");

        UserInfo user = null;
        if (Validates.isMobile11(userName))
        {//手机号
            user = ORM.get(ZTable.class).item(UserInfo.class, new Selector("userMobile", userName));
        }
        else if (Validates.isEmail(userName))
        {//邮箱格式
            user = ORM.get(ZTable.class).item(UserInfo.class, new Selector("userEmail", userName));
        }
        else
        {//其他不支持
            request.returnHistory("版主必须是注册的邮箱或手机号");
            return;
        }
        
        if(user == null)
        {
            request.setResponseError("填写的版主未注册或不存在");
            return;
        }
        
        BbsBoard board = new BbsBoard();
        board.setBoardId(Ids.longId());
        board.setBoardName(boardName);
        board.setBoardManager(user.getUserId());
        board.setBoardDesc(boardDesc);

        ORM.get(ZTable.class).insert(board);
    }

    @Override
    protected void update(HttpRequest request) throws Exception
    {
        long boardId = request.getParameterLong("boardId");
        String boardName = request.getParameter("boardName");
        String userName = request.getParameter("boardManager");
        String boardDesc = request.getParameter("boardDesc");

        UserInfo user = null;
        if (Validates.isMobile11(userName))
        {//手机号
            user = ORM.get(ZTable.class).item(UserInfo.class, new Selector("userMobile", userName));
        }
        else if (Validates.isEmail(userName))
        {//邮箱格式
            user = ORM.get(ZTable.class).item(UserInfo.class, new Selector("userEmail", userName));
        }
        else
        {//其他不支持
            request.returnHistory("版主必须是注册的邮箱或手机号");
            return;
        }
        
        if(user == null)
        {
            request.setResponseError("填写的版主未注册或不存在");
            return;
        }

        Updater updater = new Updater();
        updater.addMust("boardId", boardId);
        updater.addField("boardName", boardName);
        updater.addField("boardManager", user.getUserId());
        updater.addField("boardDesc", boardDesc);

        ORM.get(ZTable.class).update(BbsBoard.class, updater);
    }

    @Override
    protected void delete(HttpRequest request) throws Exception
    {
        long boardId = request.getParameterLong("boardId");
        ORM.get(ZTable.class).delete(BbsBoard.class, boardId);
    }
}
